# Edukit3 Microbit
Control the CamJam Edukit3 with a micro:bits acceleromoter.

# Materials
* 2 Micro:bits
* 1 Raspberry Pi
* 1 CamJam EduKit 3

# Instructions
* Build the basic edukit3 (wheels, motors, and pi)
* Flash `mb_controler.py` to a micro:bit. This will need to be battery powered
* Flash `mb_donlge.py` to a second micro:bit. This will stay pluged into the raspberry pi 
* On the pi `pip3 install gpiozero microbitdongle` or if you have pipenv  installed run `pipenv install` then `pipenv shell`
* On the pi run main.py while the second micro:bit is pluged in
* Tilting the first micro:bit should now move the robot

A more detailed writeup can be found [https://lukespademan.com/posts/2020/01/microbit-controlled-robot/](here).
