import time
from gpiozero import CamJamKitRobot
from microbitdongle import Dongle


def main():
    mb = Dongle(debug=True)
    robot = CamJamKitRobot()
    commands = {
        "l": robot.left,
        "u": robot.forward,
        "d": robot.backward,
        "r": robot.right,
        "f": robot.stop,
    }
    while True:
        data = mb.recv()
        if data:
            print(data)
            if data in commands:
                commands[data]()


if __name__ == "__main__":
    main()
